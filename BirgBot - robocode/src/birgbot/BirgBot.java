package birgbot;

import java.awt.Color;
import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

/*
 * BirgBot - a robot by Eduardo Wanderley
 */

public class BirgBot extends AdvancedRobot {

	double move;
	String enemy;
	int counter = 0;

	public void run() {

		setBodyColor(Color.pink);
		setGunColor(Color.white);
		setBulletColor(Color.cyan);
		setScanColor(Color.blue);
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);

		move = Math.max(getBattleFieldWidth(), getBattleFieldHeight());

		enemy = null;

		setTurnRadarRight(Double.POSITIVE_INFINITY);
		turnLeft(getHeading() % 90);
		ahead(move);
		turnRight(90);

		while (true) {
			ahead(move);
			turnRight(90);
			counter++;

			if (counter > 1) {
				enemy = null;
			}

		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {

		if (enemy != null && !e.getName().equals(enemy)) {
			return;
		}

		if (enemy == null) {
			enemy = e.getName();
			counter = 0;
		}

		setMaxVelocity(Math.random() * 10);

		double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
		setTurnGunRightRadians(Utils.normalRelativeAngle(absoluteBearing - getGunHeadingRadians()
				+ (e.getVelocity() * Math.sin(e.getHeadingRadians() - absoluteBearing) / 13.0)));

		distanceFire(e.getDistance());

		scan();
	}

	public void onHitRobot(HitRobotEvent e) {
		enemy = e.getName();
		double absoluteBearing = getHeadingRadians() + e.getBearingRadians();
		setTurnGunRight(absoluteBearing);
		fatalFire(e.getEnergy());
		toPutAway(e);
	}

	private void toPutAway(HitRobotEvent e) {
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			setBack(100);
		} else {
			setAhead(100);
		}
		execute();
	}

	public void onHitByBullet(HitByBulletEvent e) {
		enemy = e.getName();
	}

	public void onWin() {
		dance();
	}

	public void distanceFire(double distance) {
		if (distance > 200 || getEnergy() < 15) {
			setFire(1);
		} else if (distance > 50) {
			setFire(2);
		} else {
			setFire(3);
		}
	}

	public void fatalFire(double energy) {
		double fire = (energy / 4) + 1;
		setFire(fire);
	}

	public void dance() {
		setMaxVelocity(5);
		setTurnGunRight(10000);
		while (true) {
			setAhead(20);
			setBack(20);
			if (getEnergy() > 0.1) {
				setFire(0.1);
			}
			execute();
		}
	}
}
