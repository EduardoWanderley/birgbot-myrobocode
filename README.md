# BirgBot - My Robocode

* ## What's special about it?
    BirgBot was built to survive as long as possible in battle. Performs a safe movement, moving only around the walls, which allows you to shoot from a safe distance and at the same time remains in a very favorable position.

# Strategy

* ## How does it move?
    It always moves around the wall, and at random speed with each loop.

* ## How does it fire?
    It implements a simple non-literary linear segmentation code, which assumes that the target will continue in the same direction and at the same speed, and will shoot there.

* ## How does it dodge bullets?
    Well, it just keeps moving around the wall at random speed, making it difficult for the opponent's robot to hit the shots.

* ## How does he select a target to attack / avoid in close combat?

    -Right at the start of the game it scans the arena for a target, when finds it, keeps the target's name and keeps shooting at it until it dies or is not scanned for more than one loop. The target can also be changed if an opposing robot collides / shoots BirgBot.

# Additional Information

* ## Where did you get the name?
    I usually use 'Birguiton' as a nickname in the games I like to play. With Birgbot I tried to play a "word game" with Birguiton and Robot.

* ## What other robot(s) is it based on?
    The BirgBot movement was inspired by the SampleRobot Walls.

# Strength :

I would say that BirgBot's strong point is undoubtedly its ability to survive for a long time in the arena. In addition to its movement allowing it to stay away from confusion, it also allows a better angle to view and attack enemies.

# Weakness :

BirgBot is not effective against border guard robots or robots that "chase" the enemy. But his main drawback is that he is practically useless in very small arenas. 

# what did I most like to learn during the construction of BirgBot?

For sure building BirgBot was a unique experience, what excited me the most during the creation was the opportunity to put into practice all the knowledge I had acquired in programming logic so far and apply it in a practical and funny. In addition, with the development of my robot I was able to improve my research and development skills, contributing a lot to my journey as a developer.
